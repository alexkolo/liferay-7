package com.portlet.provider.url.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.portlet.provider.url.constants.UrlPrinterPortletProviderPortletKeys;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author Alex Colodrero
 * 
 * To print Current Url
 * 
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.css-class-wrapper=portlet-language",
		"com.liferay.portlet.display-category=category.tools",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.name=" + UrlPrinterPortletProviderPortletKeys.UrlPrinterPortletProvider,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=guest,power-user,user"
	},
	service = Portlet.class
)
public class UrlPrinterPortletProviderPortletAction extends MVCPortlet {
	
	private static Log _log = LogFactoryUtil.getLog(UrlPrinterPortletProviderPortletAction.class);

	@Override
	protected void doDispatch(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {
		
		_log.debug("Into doDispatch");
		
		printCurrentUrl(renderRequest);
		
	}

	/**
	 * 
	 * To print the current Url Extracted from Theme Display
	 * 
	 * @param renderRequest
	 */
	private void printCurrentUrl(RenderRequest renderRequest) {
		
		ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		_log.info("Visited url:" + themeDisplay.getURLCurrent());
		
	}

}