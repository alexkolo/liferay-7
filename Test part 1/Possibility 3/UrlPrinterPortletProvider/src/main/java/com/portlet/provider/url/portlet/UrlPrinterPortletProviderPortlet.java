package com.portlet.provider.url.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.BasePortletProvider;
import com.liferay.portal.kernel.portlet.ViewPortletProvider;
import com.portlet.provider.url.constants.UrlPrinterPortletProviderPortletKeys;

import org.osgi.service.component.annotations.Component;

/**
 * @author Alex Colodrero
 * 
 * 
 * To provide to the template the portlet location to extract the url
 * 
 */
@Component(
	immediate = true,
	property = "model.class.name=com.portlet.provider.url.portlet.UrlPrinterPortletProviderPortlet",
	service =  ViewPortletProvider.class
)
public class UrlPrinterPortletProviderPortlet extends BasePortletProvider implements ViewPortletProvider {

	private static Log _log = LogFactoryUtil.getLog(UrlPrinterPortletProviderPortlet.class);
	
	@Override
	public String getPortletName() {
		
		_log.debug(" Into get PortletName" );
		
		return UrlPrinterPortletProviderPortletKeys.UrlPrinterPortletProvider;
	}

//	@Override
//	public PortletURL getPortletURL(HttpServletRequest request, Group group)
//		throws PortalException {
//
//		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
//
//		return PortletURLFactoryUtil.create(request, getPortletName(), themeDisplay.getLayout(), PortletRequest.RENDER_PHASE);
//
//	}

}