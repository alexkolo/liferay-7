package com.filter.general.portlet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.osgi.service.component.annotations.Component;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;


/**
 * @author Alex Colodrero
 * 
 * To print Url Calls from the Pages (/web)
 * 
 */
@Component(
	immediate = true,
	property = {
		"servlet-context-name=", 
		"servlet-filter-name=Home Filter",
		"url-pattern=/web/*"
	},
	service = Filter.class	
)
public class PagesHomeFilter extends BaseFilter {

	private static final Log _log = LogFactoryUtil.getLog(PagesHomeFilter.class);
	
	@Override
	protected Log getLog() {
		return _log;
	}
	
	/**
	 * 
	 * The filter entry
	 * 
	 */
	@Override
	protected void processFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws Exception {

		printPageVisited(request);
		
		super.processFilter(request, response, filterChain);
		
	}

	/**
	 * 
	 * To print the url requested
	 * 
	 * @param request
	 * @return
	 */
	private void printPageVisited(HttpServletRequest request) {
		
		StringBuffer requestURL = request.getRequestURL();
		
		if (request.getQueryString() != null) {
		    requestURL.append("?").append(request.getQueryString());
		}
		
		String completeURL = requestURL.toString();
		
		_log.info("The url page Visited:" + completeURL);
	}

}