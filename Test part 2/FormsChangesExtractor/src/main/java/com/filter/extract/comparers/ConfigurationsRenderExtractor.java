package com.filter.extract.comparers;

import java.util.HashMap;
import java.util.Map;
import javax.portlet.RenderRequest;

import com.filter.constants.ChangeSettingsDetectorPortletKeys;
import com.filter.utils.Utils;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.settings.CompanyServiceSettingsLocator;
import com.liferay.portal.kernel.settings.ParameterMapSettingsLocator;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.sso.cas.configuration.CASConfiguration;
import com.liferay.portal.security.sso.cas.constants.CASConfigurationKeys;
import com.liferay.portal.security.sso.cas.constants.CASConstants;
import com.liferay.portal.security.sso.ntlm.configuration.NtlmConfiguration;
import com.liferay.portal.security.sso.ntlm.constants.NtlmConfigurationKeys;
import com.liferay.portal.security.sso.ntlm.constants.NtlmConstants;


/**
 * 
 * @author Alex Colodrero
 * 
 * 	Extractor of configurations in render Phase and putter into session
 *
 */
public class ConfigurationsRenderExtractor {
	
	private static Log _log = LogFactoryUtil.getLog(ConfigurationsRenderExtractor.class);

	/**
	 * 
	 * Trunkal method 
	 * 
	 * @param request
	 */
    public void extractConfigsAndInsertIntoSession(RenderRequest request) {
    	
    	_log.debug("Into extractConfigsAndInsertIntoSession");
    	
    	ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);

    	Map <String, String> casConfiguration = casConfigurationsExtractor(themeDisplay, request);
    	Map <String, String> ntlmConfiguration = ntlmconfigurationExtractor(themeDisplay, request);

		if(!casConfiguration.isEmpty() && !ntlmConfiguration.isEmpty()) {
			Utils.setConfigurationsIntoSession(request, ChangeSettingsDetectorPortletKeys.CAS_SESSION_ID, casConfiguration);  			
			Utils.setConfigurationsIntoSession(request, ChangeSettingsDetectorPortletKeys.NTLM_SESSION_ID, ntlmConfiguration);
		}
	}

    /**
     * 
     * To extract the configuration of CAS 
     * 
     * @param themeDisplay
     * @param request
     * @return
     */
	private Map<String, String> casConfigurationsExtractor(ThemeDisplay themeDisplay, RenderRequest request) {
		
		_log.debug("Into casConfigurationsExtractor");
		
		Map <String, String> toReturnMap = new HashMap<>();
		
		String casFormParameterNamespace = ChangeSettingsDetectorPortletKeys.CAS_FORM_PARAMETER_NAMESPACE;
		
		try {

			CASConfiguration casConfiguration = 
					ConfigurationProviderUtil.getConfiguration(CASConfiguration.class, new ParameterMapSettingsLocator(request.getParameterMap(), 
							ChangeSettingsDetectorPortletKeys.CAS_FORM_PARAMETER_NAMESPACE, new CompanyServiceSettingsLocator(themeDisplay.getCompanyId(), CASConstants.SERVICE_NAME)));

			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.AUTH_ENABLED, String.valueOf(casConfiguration.enabled()));
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.IMPORT_FROM_LDAP, String.valueOf(casConfiguration.importFromLDAP()));
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGIN_URL, casConfiguration.loginURL());
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGOUT_ON_SESSION_EXPIRATION, String.valueOf(casConfiguration.logoutOnSessionExpiration()));
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGOUT_URL, casConfiguration.logoutURL());
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.NO_SUCH_USER_REDIRECT_URL, casConfiguration.noSuchUserRedirectURL());
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVER_NAME, casConfiguration.serverName());
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVER_URL, casConfiguration.serverURL());
			toReturnMap = Utils.mapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVICE_URL, casConfiguration.serviceURL());
			
		} catch (ConfigurationException e) {
			_log.warn("Fail during ConfigurationException" + e);
		}		
		
		return toReturnMap;
		
	}
	
	
    /**
     * 
     * To extract the configuration of NTLM
     * 
     * @param themeDisplay
     * @param request
     * @return
     */
	private Map<String, String> ntlmconfigurationExtractor(ThemeDisplay themeDisplay, RenderRequest request) {
		
		_log.debug("Into ntlmconfigurationExtractor");
		
		Map <String, String> toReturnMap = new HashMap<>();
		
		String ntlmFormParameterNamespace = ChangeSettingsDetectorPortletKeys.NTLM_FORM_PARAMETER_NAMESPACE;
		
		try {

			NtlmConfiguration ntlmConfiguration = 
					ConfigurationProviderUtil.getConfiguration(NtlmConfiguration.class, new ParameterMapSettingsLocator(request.getParameterMap(), 
							ChangeSettingsDetectorPortletKeys.NTLM_FORM_PARAMETER_NAMESPACE, new CompanyServiceSettingsLocator(themeDisplay.getCompanyId(), NtlmConstants.SERVICE_NAME)));

			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_ENABLED, String.valueOf(ntlmConfiguration.enabled()));
			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN_CONTROLLER, ntlmConfiguration.domainController());
			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN_CONTROLLER_NAME, ntlmConfiguration.domainControllerName());
			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN, ntlmConfiguration.domain());
//			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_NEGOTIATE_FLAGS, ntlmConfiguration.negotiateFlags()); 
			// Null in DXP 7.0.10 GA1 / In liferay-ce-portal-7.0-ga5 is Valid
			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_SERVICE_ACCOUNT, ntlmConfiguration.serviceAccount());
//			toReturnMap = Utils.mapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_SERVICE_PASSWORD, ntlmConfiguration.servicePassword()); 
			// problems due ofuscated in post and "test" from configuration (DXP) - It makes difference that detect the comparator
			
		} catch (ConfigurationException e) {
			_log.warn("Fail during ntlmconfigurationExtractor" + e);
		}

		return toReturnMap;
		
	}


}
