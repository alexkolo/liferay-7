package com.filter.extract.comparers;

import java.util.HashMap;
import java.util.Map;
import javax.portlet.ActionRequest;

import com.filter.constants.ChangeSettingsDetectorPortletKeys;
import com.filter.utils.Utils;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.sso.cas.constants.CASConfigurationKeys;
import com.liferay.portal.security.sso.ntlm.constants.NtlmConfigurationKeys;



/**
 * 
 * Extractor of posted in Action Phase, and comparer with the saved in session.
 * 
 * @author Alex Colodrero
 *
 */
public class ConfigurationsAndPostedComparer {
	
	private static Log _log = LogFactoryUtil.getLog(ConfigurationsAndPostedComparer.class);
	

	/**
	 * 
	 * Trunkal method 
	 * 
	 * @param actionRequest
	 */
    @SuppressWarnings("unchecked")
	public void compareConfigurationOldVsPosted(ActionRequest actionRequest) {
    	
    	_log.debug("Into compareConfigurationOldVsPosted");
    	    	
		try {
				
			Map <String, String> casConfiguration = 
					(Map<String, String>) Utils.getConfigurationsFromSession(actionRequest, ChangeSettingsDetectorPortletKeys.CAS_SESSION_ID);
			Map <String, String> ntlmConfiguration = 
					(Map<String, String>) Utils.getConfigurationsFromSession(actionRequest, ChangeSettingsDetectorPortletKeys.NTLM_SESSION_ID);

			if(!casConfiguration.isEmpty() && !ntlmConfiguration.isEmpty()) {
				Map <String, String> casPosted = casPostedExtractor(actionRequest);
				Map <String, String> ntlmPosted = ntlmPostedExtractor(actionRequest);

				checkIfMapsEquals(casPosted, casConfiguration);
				checkIfMapsEquals(ntlmPosted, ntlmConfiguration);
			}
			
  		} catch (Exception e) {
			_log.warn("Fail during compareConfigurationOldVsPosted:" + e);
		}
		
	}

    /**
     * 
     * To extract the posted in the CAS form
     * 
     * @param actionRequest
     * @return
     */
	private Map<String, String> casPostedExtractor(ActionRequest actionRequest) {
		
		_log.debug("Into casPostedExtractor");
		
		Map <String, String> toReturnMap = new HashMap<>();
		
		String casFormParameterNamespace = ChangeSettingsDetectorPortletKeys.CAS_FORM_PARAMETER_NAMESPACE;

		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.AUTH_ENABLED, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.IMPORT_FROM_LDAP, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGIN_URL, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGOUT_ON_SESSION_EXPIRATION, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.LOGOUT_URL, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.NO_SUCH_USER_REDIRECT_URL, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVER_NAME, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVER_URL, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, casFormParameterNamespace + CASConfigurationKeys.SERVICE_URL, actionRequest);
		
		return toReturnMap;	
		
	}

    /**
     * 
     * To extract the posted in the NTLM form
     * 
     * @param actionRequest
     * @return
     */
	private Map<String, String> ntlmPostedExtractor(ActionRequest actionRequest) {
		
		_log.debug("Into ntlmPostedExtractor");
		
		Map <String, String> toReturnMap = new HashMap<>();
		
		String ntlmFormParameterNamespace = ChangeSettingsDetectorPortletKeys.NTLM_FORM_PARAMETER_NAMESPACE;
		
		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_ENABLED, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN_CONTROLLER, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN_CONTROLLER_NAME, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_DOMAIN, actionRequest);
//		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_NEGOTIATE_FLAGS, actionRequest);
		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_SERVICE_ACCOUNT, actionRequest);
//		toReturnMap = Utils.postedMapPutter(toReturnMap, ntlmFormParameterNamespace + NtlmConfigurationKeys.AUTH_SERVICE_PASSWORD, actionRequest);

		return toReturnMap;
		
	}
	
    /**
     * 
     * To check if the values of the maps posted and getted in configuration are the same or have differences
     * 
     * @param actionRequest
     * @return
     */
	private void checkIfMapsEquals(Map<String, String> mapPosted, Map<String, String> mapConfig) {
    	
		_log.debug("Into checkIfMapsEquals");
		
    	MapDifference<String, String> diff = Maps.difference(mapPosted, mapConfig);

    	Map<String, ValueDifference<String>> entriesDiffering = diff.entriesDiffering();
    	
    	if(!entriesDiffering.isEmpty()) {
    		printTheValuesChanged(entriesDiffering);
    	}

	}

	/**
	 * 
	 * To print the differences between maps
	 * 
	 * @param entriesDiffering
	 */
	private void printTheValuesChanged(Map<String, ValueDifference<String>> entriesDiffering) {
		
		_log.debug("Into printTheValuesChanged");
		
    	for (String keys : entriesDiffering.keySet()) {
    		_log.info("Id del elemento modificado:" + keys);
    		ValueDifference<String> valueDifference = entriesDiffering.get(keys);
    		_log.info("Valores:");
    		_log.info("- Actual:" + valueDifference.leftValue());
    		_log.info("- Anterior:" + valueDifference.rightValue());
    	}
		
	}

}
