package com.filter.jsp;

import java.io.IOException;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;
import javax.portlet.filter.RenderFilter;
import org.osgi.service.component.annotations.Component;

import com.filter.extract.comparers.ConfigurationsRenderExtractor;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortletKeys;

/**
 * @author Alex Colodrero
 * 
 * To filter the render phase of the Portlet ; PortalSettingsPortlet
 */
@Component(
	immediate = true,
	property = {
			"javax.portlet.name=" + PortletKeys.PORTAL_SETTINGS
	},
	service = PortletFilter.class
)

/**
 * 
 * To filter the render phase of the Portlet ; PortalSettingsPortlet
 * @author Alex Colodrero
 *
 */
public class InstanceSettingsRenderFilter implements RenderFilter {
	
	private static Log _log = LogFactoryUtil.getLog(InstanceSettingsRenderFilter.class);

	@Override
	public void init(FilterConfig config) throws PortletException {
	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain)
			throws IOException, PortletException {

		_log.debug("Into Do Filter");

		try {
			
			ConfigurationsRenderExtractor initExtractor = new ConfigurationsRenderExtractor();		
			initExtractor.extractConfigsAndInsertIntoSession(request);
			
		} catch (Exception e) {
			_log.warn("Problem during doFilter" + e);
		}
		
		chain.doFilter(request, response);

	}

}
