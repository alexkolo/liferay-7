package com.filter.jsp;

import java.io.IOException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;

import com.filter.extract.comparers.ConfigurationsAndPostedComparer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortletKeys;

/**
 * @author Alex Colodrero
 * 
 * To filter the action phase of the Portlet ; PortalSettingsPortlet
 */
@Component(
	immediate = true,
	property = {
			"javax.portlet.name=" + PortletKeys.PORTAL_SETTINGS
	},
	service = PortletFilter.class
)


public class InstanceSettingsActionFilter implements ActionFilter {

	private static Log _log = LogFactoryUtil.getLog(InstanceSettingsActionFilter.class);
	
	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain)
			throws IOException, PortletException {

		_log.debug("Into Do Filter");

		ConfigurationsAndPostedComparer initComparer = new ConfigurationsAndPostedComparer();
		initComparer.compareConfigurationOldVsPosted(request);
		
		chain.doFilter(request, response);

	}

}
