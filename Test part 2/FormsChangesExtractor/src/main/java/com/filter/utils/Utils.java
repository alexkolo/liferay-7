package com.filter.utils;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * @author Alex Colodrero
 * 
 * Utils Class
 */
public class Utils {
	
	private static Log _log = LogFactoryUtil.getLog(Utils.class);
	
	/**
	 * 
	 * To make reusable mapPutter
	 * 
	 * @param toReturnMap
	 * @param key
	 * @param actionRequest
	 * @return
	 */
	public static Map<String, String> postedMapPutter(Map<String, String> toReturnMap, String key, ActionRequest actionRequest) {

		_log.debug("Into postedMapPutter");
		
		String valueToInsertInMap = actionRequest.getParameter(key);
		
		return mapPutter(toReturnMap, key, valueToInsertInMap);
	}
	
	/**
	 * 
	 * To make Less verbose the PostedExtractor Methods
	 * 
	 * @param toReturnMap
	 * @param key
	 * @param actionRequest
	 * @return
	 */
	public static Map<String, String> mapPutter(Map<String, String> toReturnMap, String key, String value) {

		_log.debug("Into mapPutter");
		
		toReturnMap.put(key, value);
		
		return toReturnMap;
	}

	/**
	 * 
	 * To insert some object into portlet session
	 * 
	 * @param request
	 * @param casConfiguration
	 * @param ntlmConfiguration
	 * @return 
	 */
	public static void setConfigurationsIntoSession(PortletRequest request, String keyInSession, Map<String, String> objectToInsert) {	
		
		_log.debug("Into setConfigurationsIntoSession");
		
		PortletSession session = request.getPortletSession();			
		session.setAttribute(keyInSession, objectToInsert, PortletSession.PORTLET_SCOPE);
		
	}
	
	/**
	 * 
	 * To return some object from portlet session
	 * 
	 * @param request
	 * @param keyInSession
	 * @return
	 */
	public static Object getConfigurationsFromSession(PortletRequest request, String keyInSession) {	
		
		_log.debug("Into getConfigurationsFromSession");
		
		PortletSession session = request.getPortletSession();		
		return session.getAttribute(keyInSession);
		
	}

}
