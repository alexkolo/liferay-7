package com.filter.constants;

/**
 * @author Alex Colodrero
 */
public class ChangeSettingsDetectorPortletKeys {

	public static final String PORTAL_SETTINGS = "com_liferay_portal_settings_web_portlet_PortalSettingsPortlet";
	
	public static final String CAS_FORM_PARAMETER_NAMESPACE = "cas_";
	public static final String NTLM_FORM_PARAMETER_NAMESPACE = "ntlm_";
	
	public static final String CAS_SESSION_ID = "cas_session";
	public static final String NTLM_SESSION_ID = "ntlm_session";

}